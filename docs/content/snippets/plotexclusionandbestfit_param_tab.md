The `PlotExclusionAndBestFit` task collects the fit results from the `MergeUpperLimits` and `MergeLikelihoodScan` tasks and visualizes them in a plot.

<div class="dhi_parameter_table">

--8<-- "content/snippets/parameters.md@-2,21,19,16,54,14,66,12,55,72,73,48,17,18,74,3,4,22,23,12,5,6"

</div>
